/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.client.BlastServerClient;

/**
 *
 * @author dhudson - Mar 21, 2017 - 3:55:50 PM
 */
public interface Topic {
    
    public String getName();
    
    public void sendMessage(SendTopicMessage message);
    
    public void subscribe(BlastServerClient client);
    
    public void unSubscribe(BlastServerClient client);
}
