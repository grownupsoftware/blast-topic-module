/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.client.BlastServerClient;

/**
 *
 * @author dhudson - Mar 21, 2017 - 3:14:57 PM
 */
public interface TopicManager {

    public void addTopic(Topic topic);
    
    public void removeTopic(Topic topic);
 
    public void handleOnMessage(BlastServerClient client, SendTopicMessage message);
}
