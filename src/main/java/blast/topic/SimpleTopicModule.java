/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.module.BlastModule;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Mar 23, 2017 - 4:43:24 PM
 */
public class SimpleTopicModule implements BlastModule {

    public static final String TOPIC_MODULE_ID = "co.gusl.topic";

    private final TopicManager topicManager;

    public SimpleTopicModule() {
        topicManager = new SimpleTopicManagerImpl();
    }

    @Override
    public void configure(BlastServer server) {
        // Configure all command handles here ..        
        //server.setTopicManager(topicManager);

        // Register commands ..
        server.registerCommand("send", SendTopicMessageEvent.class, SendTopicMessage.class);
        server.registerCommand("subscribe", SubscribeMessageEvent.class, TopicMessage.class);
        server.registerCommand("unsubscribe", UnSubscribeMessageEvent.class, TopicMessage.class);

        // Lets get the topic manager to listen to the event bus
        server.getEventBus().register(topicManager);
    }

    @Override
    public String getModuleID() {
        return TOPIC_MODULE_ID;
    }

}
