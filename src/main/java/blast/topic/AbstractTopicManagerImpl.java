/*
 * Grownup Software Limited.
 */
package blast.topic;

/**
 *
 * @author dhudson - Mar 21, 2017 - 3:15:09 PM
 */
public abstract class AbstractTopicManagerImpl implements TopicManager {

    public AbstractTopicManagerImpl() {
    }

    public abstract Topic getTopic();

    public abstract Topic getTopic(String topicPath);
    
}
