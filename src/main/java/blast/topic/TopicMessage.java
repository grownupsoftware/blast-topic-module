/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.message.CommandMessage;

/**
 *
 * @author dhudson - Mar 23, 2017 - 3:01:43 PM
 */
public class TopicMessage extends CommandMessage {

    public String topic;

    public TopicMessage() {

    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
