/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.client.BlastServerClient;
import blast.eventbus.OnEvent;
import java.util.HashMap;

/**
 *
 * @author dhudson - Mar 21, 2017 - 3:57:16 PM
 */
public class SimpleTopicManagerImpl extends AbstractTopicManagerImpl {

    private final HashMap<String, Topic> topicMap;

    public SimpleTopicManagerImpl() {
        topicMap = new HashMap<>(5);
    }

    @Override
    public Topic getTopic() {
        return null;
    }

    @Override
    public Topic getTopic(String topicPath) {
        return topicMap.get(topicPath);
    }

    @Override
    public void addTopic(Topic topic) {
        topicMap.put(topic.getName(), topic);
    }

    @Override
    public void removeTopic(Topic topic) {
        topicMap.remove(topic.getName());
    }

    @Override
    public void handleOnMessage(BlastServerClient client, SendTopicMessage message) {
        Topic topic = topicMap.get(message.getTopic());
        if (topic != null) {
            topic.sendMessage(message);
        }
    }

    @OnEvent
    public void handleInboundMessageEvent(SendTopicMessageEvent message) {
        handleOnMessage(message.getClient(), message.getCommandData());
    }

    @OnEvent
    public void handleSubscribe(SubscribeMessageEvent event) {
        TopicMessage message = event.getCommandData();
        Topic topic = getTopic(message);
        if (topic != null) {
            topic.subscribe(event.getClient());
        } else {
            // Grumble ..

        }
    }

    @OnEvent
    public void handleUnsubscribe(UnSubscribeMessageEvent event) {
        TopicMessage message = event.getCommandData();
        Topic topic = getTopic(message);
        if (topic != null) {
            topic.unSubscribe(event.getClient());
        } else {
            // Grumble ..

        }
    }

    private Topic getTopic(TopicMessage message) {
        return topicMap.get(message.getTopic());
    }
}
