/*
 * Grownup Software Limited.
 */
package blast.topic;

import blast.client.BlastServerClient;
import blast.log.BlastLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author dhudson - Mar 21, 2017 - 3:55:58 PM
 */
public class SimpleTopicImpl implements Topic {

    private static final BlastLogger logger = BlastLogger.createLogger();

    private final String name;
    private final Map<String, BlastServerClient> subscriptionMap;
    private final ObjectMapper objectMapper;

    public SimpleTopicImpl(String name, ObjectMapper mapper) {
        this.name = name;
        subscriptionMap = new ConcurrentHashMap<>(5);
        objectMapper = mapper;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(SendTopicMessage message) {
        // rip through the subscriptions and send the message
        byte[] bytes;
        try {
            bytes = objectMapper.writeValueAsBytes(message);
        } catch (JsonProcessingException ex) {
            logger.warn("Unable to decode POJO to JSON", ex);
            return;
        }
        for (BlastServerClient client : subscriptionMap.values()) {
            try {
                client.write(bytes);
            } catch (IOException ex) {
                // Client gone away ...
            }
        }
    }

    @Override
    public void subscribe(BlastServerClient client) {
        subscriptionMap.put(client.getClientID(), client);
    }

    @Override
    public void unSubscribe(BlastServerClient client) {
        subscriptionMap.remove(client.getClientID());
    }

}
