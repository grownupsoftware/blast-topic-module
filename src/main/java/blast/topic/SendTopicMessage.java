/*
 * Grownup Software Limited.
 */
package blast.topic;

/**
 *
 * @author dhudson - Mar 23, 2017 - 3:03:49 PM
 */
public class SendTopicMessage extends TopicMessage {

    public String message;
    public String type;

    public SendTopicMessage() {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
